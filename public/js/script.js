const submitButton = document.querySelector(".card__submit");
const ratingsContainer = document.querySelector(".card__ratings");
const cardRatingContainer = document.querySelector(".card__container--rating-state");
const thankYouContainer = document.querySelector(".card__container--thanks-state");
const selectedRatingComponent = document.querySelector(".card__selected-rating-value");

ratingsContainer.addEventListener("click", event => {
    let selectedRating = event.target.innerText;
    if (selectedRating > 0 || selectedRating <= 5){
        submitButton.addEventListener("click", event => {
            selectedRatingComponent.innerText = selectedRating;
            cardRatingContainer.style.display = "none";
            thankYouContainer.style.display = "flex";
        });
    }
});




